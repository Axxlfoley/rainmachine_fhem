###############################################################################
# 
#  (c) 2018 Copyright: Marcus Beckerle (marcus.beckerle at gmx dot de)
#  All rights reserved
#
#
#  This script is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  The GNU General Public License can be found at
#  http://www.gnu.org/copyleft/gpl.html.
#  A copy is found in the textfile GPL.txt and important notices to the license
#  from the author is found in LICENSE.txt distributed with these scripts.
#
#  This script is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#
# $Id: 73_RainMachine.pm 15650 2017-12-20 05:41:22Z CoolTux $
#
###############################################################################
##
##
## Das JSON Modul immer in einem eval aufrufen
# $data = eval{decode_json($data)};
#
# if($@){
#   Log3($SELF, 2, "$TYPE ($SELF) - error while request: $@");
#  
#   readingsSingleUpdate($hash, "state", "error", 1);
#
#   return;
# }
#
#
#   
##
##



package main;


my $missingModul = "";

use strict;
use warnings;

use HttpUtils;

eval "use Encode qw(encode encode_utf8 decode_utf8);1" or $missingModul .= "Encode ";
eval "use JSON;1" or $missingModul .= "JSON ";
eval "use IO::Socket::SSL;1" or $missingModul .= "IO::Socket::SSL ";

my $version = "4";

# Declare functions
sub RainMachine_Attr(@);
sub RainMachine_Define($$);
sub RainMachine_Initialize($);
sub RainMachine_Set($@);
sub RainMachine_Write($@);
sub RainMachine_Undef($$);
sub RainMachine_ResponseProcessing($$);
sub RainMachine_ErrorHandling($$$);
sub RainMachine_encrypt($);
sub RainMachine_decrypt($);
sub RainMachine_WriteReadings($$);
sub RainMachine_ParseJSON($$);
sub RainMachine_getDevices($);
sub RainMachine_getToken($);
sub RainMachine_InternalTimerGetDeviceData($);
sub RainMachine_createHttpValueStrings($@);

# Initialization
sub RainMachine_Initialize($) {

    my ($hash) = @_;
    
    # Provider
    $hash->{WriteFn}    = "RainMachine_Write";
    $hash->{Clients}    = ":RainMachineDevice:";
    $hash->{MatchList}  = { "1:RainMachineDevice"      => '^{"id":".*' };

    # Consumer
    $hash->{SetFn}      = "RainMachine_Set";
    $hash->{DefFn}      = "RainMachine_Define";
    $hash->{UndefFn}    = "RainMachine_Undef";
    $hash->{AttrFn}     = "RainMachine_Attr";

    # Attributes, controlled by RainMachine_Attr
    $hash->{AttrList}   = "debugJSON:0,1 ".
                          "disable:1 ".
                          "interval ".
                          "disabledForIntervals ".
                          $readingFnAttributes;
    
    foreach my $d(sort keys %{$modules{RainMachine}{defptr}}) {
        my $hash = $modules{RainMachine}{defptr}{$d};
        $hash->{VERSION}      = $version;
    }
}

# Called on definition of devide: initialize device data
sub RainMachine_Define($$) {

    my ( $hash, $def ) = @_;
    
    my @a = split( "[ \t][ \t]*", $def );

    
    return "too few parameters: define <NAME> RainMachine <Passwort> <Server:Port>" if( @a != 4 ) ;
    return "Cannot define RainMachine device. Perl modul ${missingModul}is missing." if ( $missingModul );

    # Get parameters
    my $name                = $a[1];
    my $password            = $a[2];
    my $url                 = $a[3];

    # Set internals
    $hash->{BRIDGE}         = 1;
    $hash->{URL}            = $url . '/api/' . $version;
    $hash->{VERSION}        = $version;
    $hash->{INTERVAL}       = 300;

    # Save password encrypted
    my $encryptedPassword            = RainMachine_encrypt($password);
    Log3 $name, 3, "RainMachine ($name) - encrypt $password to $encryptedPassword" if($password ne $encryptedPassword);
    $hash->{DEF} = "$encryptedPassword";
    $hash->{helper}{encryptedPassword} = $encryptedPassword;

    # Set room if not defined
    $attr{$name}{room} = "RainMachine" if( !defined( $attr{$name}{room} ) );

    # Set initial readings
    readingsSingleUpdate($hash,'state','initialized',1);
    readingsSingleUpdate($hash,'token','none',1);
    Log3 $name, 3, "RainMachine ($name) - defined RainMachine and crypted your credentials";

    # $init_done indicates if FHEM is done starting, if so get token, else schedule getting token
    if( $init_done ) {
        RainMachine_getToken($hash);
    } else {
        InternalTimer( gettimeofday()+15, "RainMachine_getToken", $hash, 0 );
    }

    # Define pointer (defptr = definitionpointer) to be able to access the bridge from the logical devices (zones)
    $modules{RainMachine}{defptr}{BRIDGE} = $hash;

    return undef;
}

# What to do if undefining device
sub RainMachine_Undef($$) {

    my ( $hash, $arg ) = @_;

    RemoveInternalTimer($hash);
    delete $modules{RainMachine}{defptr}{BRIDGE} if( defined($modules{RainMachine}{defptr}{BRIDGE}) );

    return undef;
}

# Method that is called when setting an attribute
sub RainMachine_Attr(@) {

    my ( $cmd, $name, $attrName, $attrVal ) = @_;
    my $hash = $defs{$name};

    if( $attrName eq "disable" ) {
        if( $cmd eq "set" and $attrVal eq "1" ) {
            RemoveInternalTimer($hash) if($init_done);
            readingsSingleUpdate ( $hash, "state", "inactive", 1 );
            Log3 $name, 3, "RainMachine ($name) - disabled";
        }

        elsif( $cmd eq "del" ) {
            RainMachine_InternalTimerGetDeviceData($hash) if($init_done);
            readingsSingleUpdate ( $hash, "state", "active", 1 );
            Log3 $name, 3, "RainMachine ($name) - enabled";
        }
    }
    
    elsif( $attrName eq "disabledForIntervals" ) {
        if( $cmd eq "set" ) {
            return "check disabledForIntervals Syntax HH:MM-HH:MM or 'HH:MM-HH:MM HH:MM-HH:MM ...'"
            unless($attrVal =~ /^((\d{2}:\d{2})-(\d{2}:\d{2})\s?)+$/);
            Log3 $name, 3, "RainMachine ($name) - disabledForIntervals";
        }

        elsif( $cmd eq "del" ) {
            readingsSingleUpdate ( $hash, "state", "active", 1 );
            Log3 $name, 3, "RainMachine ($name) - enabled";
        }
    }
    
    elsif( $attrName eq "interval" ) {
        if( $cmd eq "set" ) {
            return "Interval must be greater than 0"
            unless($attrVal > 0);
            $hash->{INTERVAL}   = $attrVal;
            Log3 $name, 3, "RainMachine ($name) - set interval: $attrVal";
            RainMachine_InternalTimerGetDeviceData($hash) if($init_done);
        }

        elsif( $cmd eq "del" ) {
            $hash->{INTERVAL}   = 300;
            Log3 $name, 3, "RainMachine ($name) - delete User interval and set default: 300";
            RainMachine_InternalTimerGetDeviceData($hash) if($init_done);
        }
    }

    return undef;
}

# Method that is called when using the settings
sub RainMachine_Set($@) {
    
    my ($hash, $name, $cmd, @args) = @_;
    my ($arg, @params) = @args;
    
    
    if( lc $cmd eq 'getdevicesstate' ) {
    
        RainMachine_getDevices($hash);
        
    } elsif( lc $cmd eq 'gettoken' ) {
    
        return "token is up to date" if( defined($hash->{helper}{access_token}) );
        RainMachine_getToken($hash);
    
    } else {
    
        my $list = "getDevicesState:noArg getToken:noArg";
        return "Unknown argument $cmd, choose one of $list";
    }
    
    return undef;
}

# ???
sub RainMachine_InternalTimerGetDeviceData($) {

    my $hash    = shift;
    my $name    = $hash->{NAME};

    RemoveInternalTimer($hash);
    
    if( not IsDisabled($name) ) {
    
        RainMachine_getDevices($hash);
        Log3 $name, 4, "RainMachine ($name) - set internal timer function for recall InternalTimerGetDeviceData sub";
        
    } else {
    
        readingsSingleUpdate($hash,'state','disabled',1);
        Log3 $name, 3, "RainMachine ($name) - device is disabled";
    }
    
    InternalTimer( gettimeofday()+$hash->{INTERVAL},"RainMachine_InternalTimerGetDeviceData", $hash, 1 );
}

# Write data to RainMachine
sub RainMachine_Write($@) {

    # Parameters
    my ($hash,$payload,$deviceId,$abilities)  = @_;

    # Variables
    my $name = $hash->{NAME};
    my ($access_token,$header,$uri,$method);

    # Create parameters for API call
    ($payload,$access_token,$header,$uri,$method,$deviceId,$abilities) = RainMachine_createHttpValueStrings($hash,$payload,$deviceId,$abilities);

    Log3 $name, 3, "RainMachine ($name) - Send with URL: $hash->{URL}$uri, HEADER: $header, DATA: $payload, METHOD: $method";

    # Do call, register callback
    HttpUtils_NonblockingGet({
        url         => $hash->{URL} . $uri,
        timeout     => 15,
        hash        => $hash,
        device_id   => $deviceId,
        data        => $payload,
        method      => $method,
        header      => $header,
        doTrigger   => 1,
        callback    => \&RainMachine_ErrorHandling
    });
}

# Callback method for API calls, handles errors and forwards the request to the data processor method
sub RainMachine_ErrorHandling($$$) {

    my ($param,$err,$data)  = @_;
    
    my $hash                = $param->{hash};
    my $name                = $hash->{NAME};
    my $dhash               = $hash;
    
    $dhash                  = $modules{RainMachineDevice}{defptr}{$param->{'device_id'}}
    unless( not defined( $param->{'device_id'}) );
    
    my $dname                       = $dhash->{NAME};


   
   # General connection error handling
   if( defined( $err ) ) {
        if( $err ne "" ) {
            
            readingsBeginUpdate( $dhash );
            readingsBulkUpdate( $dhash, "state", "$err") if( ReadingsVal( $dname, "state", 1 ) ne "initialized" );

            readingsBulkUpdate( $dhash, "lastRequestState", "request_error", 1 );
        
            if( $err =~ /timed out/ ) {
        
                Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: connect to gardena cloud is timed out. check network";
            }
        
            elsif( $err =~ /Keine Route zum Zielrechner/ or $err =~ /no route to target/ ) {
        
                Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: no route to target. bad network configuration or network is down";
        
            } else {

                Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: $err";
            }

            readingsEndUpdate( $dhash, 1 );

            Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: RainMachine RequestErrorHandling: error while requesting gardena cloud: $err";

            delete $dhash->{helper}{deviceAction} if( defined($dhash->{helper}{deviceAction}) );
            
            return;
        }
    }

    Log3 $dname, 3, "RainMachine ($dname) - Response: $param->{code}, Content: $data";

    # Auth error, async answers, check if required
    if( $data eq "" and exists( $param->{code} ) && $param->{code} != 200 ) {
        
        readingsBeginUpdate( $dhash );
        readingsBulkUpdate( $dhash, "state", $param->{code}, 1 ) if( ReadingsVal( $dname, "state", 1 ) ne "initialized" );

        readingsBulkUpdateIfChanged( $dhash, "lastRequestState", "request_error", 1 );
        
        if( $param->{code} == 401  and $hash eq $dhash ) {
        
            if( ReadingsVal($dname,'token','none') eq 'none' ) {
                readingsBulkUpdate( $dhash, "state", "no token available", 1);
                readingsBulkUpdateIfChanged( $dhash, "lastRequestState", "no token available", 1 );
            }
            
            Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: ".$param->{code};
        
        } elsif( $param->{code} == 204 and $dhash ne $hash and defined($dhash->{helper}{deviceAction}) ) {
            
            readingsBulkUpdate( $dhash, "state", "the command is processed", 1);
            InternalTimer( gettimeofday()+3,"RainMachine_getDevices", $hash, 1 );
        
        } elsif( $param->{code} != 200 ) {

            Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: ".$param->{code};
        }

        readingsEndUpdate( $dhash, 1 );
        
        Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: received http code ".$param->{code}." without any data after requesting gardena cloud";
        
        delete $dhash->{helper}{deviceAction} if( defined($dhash->{helper}{deviceAction}) );

        return;
    }

    # General HTTP errors
    if( ( ($data =~ /Error/ ) or defined(eval{decode_json($data)}->{errors}) ) and exists( $param->{code} ) ) {
        readingsBeginUpdate( $dhash );
        readingsBulkUpdate( $dhash, "state", $param->{code}, 1 ) if( ReadingsVal( $dname, "state" ,0) ne "initialized" );

        readingsBulkUpdate( $dhash, "lastRequestState", "request_error", 1 );

        if( $param->{code} == 400 ) {
            if( eval{decode_json($data)} ) {
                if( ref(eval{decode_json($data)}->{errors}) eq "ARRAY" and defined(eval{decode_json($data)}->{errors}) ) {
                    readingsBulkUpdate( $dhash, "state", eval{decode_json($data)}->{errors}[0]{error} . ' ' . eval{decode_json($data)}->{errors}[0]{attribute}, 1);
                    readingsBulkUpdate( $dhash, "lastRequestState", eval{decode_json($data)}->{errors}[0]{error} . ' ' . eval{decode_json($data)}->{errors}[0]{attribute}, 1 );
                    Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: " . eval{decode_json($data)}->{errors}[0]{error} . " " . eval{decode_json($data)}->{errors}[0]{attribute};
                }
            } else {
                readingsBulkUpdate( $dhash, "lastRequestState", "Error 400 Bad Request", 1 );
                Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: Error 400 Bad Request";
            }
        } elsif( $param->{code} == 503 ) {

            Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: Error 503 Service Unavailable";
            readingsBulkUpdate( $dhash, "state", "Service Unavailable", 1 );
            readingsBulkUpdate( $dhash, "lastRequestState", "Error 503 Service Unavailable", 1 );
            
        } elsif( $param->{code} == 404 ) {
            if( defined($dhash->{helper}{deviceAction}) and $dhash ne $hash ) {
                readingsBulkUpdate( $dhash, "state", "device Id not found", 1 );
                readingsBulkUpdate( $dhash, "lastRequestState", "device id not found", 1 );
            }
            
            Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: Error 404 Not Found";
        
        } elsif( $param->{code} == 500 ) {
        
            Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: check the ???";
        
        } else {

            Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: http error ".$param->{code};
        }

        readingsEndUpdate( $dhash, 1 );
        
        Log3 $dname, 3, "RainMachine ($dname) - RequestERROR: received http code ".$param->{code}." receive Error after requesting RainMachine data";
        
        delete $dhash->{helper}{deviceAction} if( defined($dhash->{helper}{deviceAction}) );

        return;
    }
    
    



    # Finally pass wo data analyzer
    readingsSingleUpdate($hash,'state','connected to cloud',1) if( defined($hash->{helper}{locations_id}) );
    RainMachine_ResponseProcessing($hash,$data);
}

# Method that handles the API response if no error occured
sub RainMachine_ResponseProcessing($$) {

    my ($hash,$json)    = @_;
    my $name            = $hash->{NAME};

    Log3 $name, 3, "RainMachine ($name) - Received JSON: $json";

    # The decoded data
    my $decode_json =   eval{decode_json($json)};

    # This contains the error message from the last eval
    if($@){
        Log3 $name, 3, "RainMachine ($name) - JSON error while request: $@";
        
        if( AttrVal( $name, 'debugJSON', 0 ) == 1 ) {
            readingsBeginUpdate($hash);
            readingsBulkUpdate($hash, 'JSON_ERROR', $@, 1);
            readingsBulkUpdate($hash, 'JSON_ERROR_STRING', $json, 1);
            readingsEndUpdate($hash, 1);
        }
    }
    
    
    # The login call arrives here, process it and try to get the zones next
    if( defined($decode_json->{access_token}) and $decode_json->{access_token}) {
    
        $hash->{helper}{access_token}         = $decode_json->{access_token};

        Log3 $name, 3, "RainMachine ($name) - Updating token: $hash->{helper}{access_token}";
        readingsSingleUpdate($hash,'token',$hash->{helper}{access_token},1);

        # Trigger getting the zones (no argument)
        Log3 $name, 3, "RainMachine ($name) - fetching zones next";
        RainMachine_Write($hash, undef, undef, undef);
        
        return;
    
    } elsif( defined($decode_json->{zones}) and ref($decode_json->{zones}) eq "ARRAY" and scalar(@{$decode_json->{zones}}) > 0) {
    
        foreach my $zone ( @{$decode_json->{zones}} ) {
            $hash->{helper}{zone_id} = $zone->{id};
            RainMachine_WriteReadings($hash,$zone);

            # Forward zone to RainMachineDevice module as String (as regular expressions must be used to validate it)
            my $zone_json =   eval{encode_json($zone)};
			
            Dispatch($hash,$zone_json,undef);
        }
        
        Log3 $name, 3, "RainMachine ($name) - done processing zones";
        
        return;
        
    }

    Log3 $name, 3, "RainMachine ($name) - no Match for processing data"
}

# ???
sub RainMachine_WriteReadings($$) {

    my ($hash,$decode_json)     = @_;
    my $name                    = $hash->{NAME};


    if( defined($decode_json->{uid}) and $decode_json->{uid} and defined($decode_json->{name}) and $decode_json->{name} ) {

        readingsBeginUpdate($hash);
        readingsBulkUpdateIfChanged($hash,'name',$decode_json->{name});
        readingsEndUpdate( $hash, 1 );
    }

    Log3 $name, 3, "RainMachine ($name) - readings written";
}

# ???
sub RainMachine_getDevices($) {

    my $hash    = shift;
    my $name    = $hash->{NAME};

    readingsSingleUpdate($hash,'state','getting devices',1);
    RainMachine_Write($hash, undef, undef, undef);
    Log3 $name, 4, "RainMachine ($name) - fetch device list and device states";
}

# This method gets the API token after logging in into the RainMachine
sub RainMachine_getToken($) {

    my $hash    = shift;
    my $name    = $hash->{NAME};

    readingsSingleUpdate($hash,'state','get token',1);

    delete $hash->{helper}{access_token}      if( defined($hash->{helper}{access_token}) and $hash->{helper}{access_token} );

    RainMachine_Write($hash,'{"pwd": "'.RainMachine_decrypt($hash->{helper}{encryptedPassword}).'", "remember":0}',undef,undef);
    
    Log3 $name, 3, "RainMachine ($name) - send credentials to fetch Token and locationId";
    
    RemoveInternalTimer($hash);
    InternalTimer( gettimeofday()+$hash->{INTERVAL},"RainMachine_InternalTimerGetDeviceData", $hash, 1 );
}

# Encrpyt some data e.g. for saving a password
sub RainMachine_encrypt($) {

    my ($decoded) = @_;
    my $key = getUniqueId();
    my $encoded;

    return $decoded if( $decoded =~ /crypt:/ );

    for my $char (split //, $decoded) {
        my $encode = chop($key);
        $encoded .= sprintf("%.2x",ord($char)^ord($encode));
        $key = $encode.$key;
    }

    return 'crypt:'.$encoded;
}

# Decrypt some data, e.g. for getting a password
sub RainMachine_decrypt($) {

    my ($encoded) = @_;
    my $key = getUniqueId();
    my $decoded;

    return $encoded if( $encoded !~ /crypt:/ );
  
    $encoded = $1 if( $encoded =~ /crypt:(.*)/ );

    for my $char (map { pack('C', hex($_)) } ($encoded =~ /(..)/g)) {
        my $decode = chop($key);
        $decoded .= chr(ord($char)^ord($decode));
        $key = $decode.$key;
    }

    return $decoded;
}

# ???
sub RainMachine_ParseJSON($$) {

    my ($hash, $buffer) = @_;
    
    my $name    = $hash->{NAME};
    my $open    = 0;
    my $close   = 0;
    my $msg     = '';
    my $tail    = '';

    if($buffer) {
        foreach my $c (split //, $buffer) {
            if($open == $close && $open > 0) {
                $tail .= $c;
                Log3 $name, 5, "RainMachine ($name) - $open == $close && $open > 0";
            } elsif(($open == $close) && ($c ne '{')) {
                Log3 $name, 5, "RainMachine ($name) - Garbage character before message: " . $c;
            } else {
                if($c eq '{') {
                    $open++;
                } elsif($c eq '}') {
                    $close++;
                }
                $msg .= $c;
            }
        }
        if($open != $close) {
            $tail = $msg;
            $msg = '';
        }
    }
    Log3 $name, 4, "RainMachine ($name) - return msg: $msg and tail: $tail";
    return ($msg,$tail);
}

# Generate uri etc to be used in API calls
sub RainMachine_createHttpValueStrings($@) {

    # The parameters
    my ($hash,$payload,$deviceId,$abilities)  = @_;

    my $access_token                  = $hash->{helper}{access_token};
    my $header                      = "Content-Type: application/json";
    my $uri                         = '';
    my $method                      = 'POST';
    $header                         .= "\r\nX-Session: $access_token"                          if( defined($hash->{helper}{access_token}) );
    #$payload                        = '{' . $payload . '}'                                    if( defined($payload) );
    $payload                        = '{}'                                                     if( not defined($payload) );

    # If there is no content, ise GET
    if( $payload eq '{}' ) {
        $method                         = 'GET';
        $payload                        = '';
        $uri                            .= '/zone?access_token='.$access_token                 if( not defined($abilities) and defined($access_token) );
        #readingsSingleUpdate($hash,'state','fetch zones',1)                                    if( not defined($abilities) and defined($access_token) );
    }

    # Start a zone
    if( $payload eq 'start_zone' ) {
        $method = 'POST';
        $payload = '{"time": 600 }';
        $uri .= '/zone/' . $deviceId . '/start?access_token=' . $access_token;
        readingsSingleUpdate($hash, 'state', 'starting zone ' . $deviceId, 1) if (defined($access_token));
    }

    # Stop a zone
    if( $payload eq 'stop_zone' ) {
        $method = 'POST';
        $payload = '{}';
        $uri .= '/zone/' . $deviceId . '/stop?access_token=' . $access_token;
        readingsSingleUpdate($hash, 'state', 'stopping zone ' . $deviceId, 1) if (defined($access_token));
    }

    # If no session is defined, login
    $uri .= '/auth/login'                                                                      if( not defined($hash->{helper}{access_token}));

    return ($payload,$access_token,$header,$uri,$method,$deviceId,$abilities);
}




1;






=pod

=item device
=item summary       Modul to communicate with the GardenaCloud
=item summary_DE    Modul zur Datenübertragung zur GardenaCloud

=begin html

<a name="RainMachine"></a>
<h3>RainMachine</h3>
<ul>
  <u><b>Prerequisite</b></u>
  <br><br>
  <li>In combination with RainMachineDevice this FHEM Module controls the communication between the GardenaCloud and connected Devices like Mover, Watering_Computer, Temperature_Sensors</li>
  <li>Installation of the following packages: apt-get install libio-socket-ssl-perl</li>
  <li>The Gardena-Gateway and all connected Devices must be correctly installed in the GardenaAPP</li>
</ul>
<br>
<a name="RainMachinedefine"></a>
<b>Define</b>
<ul><br>
  <code>define &lt;name&gt; RainMachine &lt;Account-EMail&gt; &lt;Account-Passwort&gt;</code>
  <br><br>
  Beispiel:
  <ul><br>
    <code>define Gardena_Bridge RainMachine me@email.me secret</code><br>
  </ul>
  <br>
  &lt;Account-EMail&gt; Email Adresse which was used in the GardenaAPP<br>
  &lt;Account-Passwort&gt; Passwort which was used in the GardenaAPP<br>
  The RainMachine device is created in the room RainMachine, then the devices of Your system are recognized automatically and created in FHEM. From now on the devices can be controlled and changes in the GardenaAPP are synchronized with the state and readings of the devices.
  <br><br>
  <a name="RainMachinereadings"></a>
  <br><br>
  <b>Readings</b>
  <ul>
    <li>address - your Adress (Longversion)</li>
    <li>authorized_user_ids - </li>
    <li>city - Zip, City</li>
    <li>devices - Number of Devices in the Cloud (Gateway included)</li>
    <li>lastRequestState - Last Status Result</li>
    <li>latitude - Breitengrad des Grundstücks</li>
    <li>longitude - Längengrad des Grundstücks</li>
    <li>name - Name of your Garden – Default „My Garden“</li>
    <li>state - State of the Bridge</li>
    <li>token - SessionID</li>
    <li>zones - </li>
  </ul>
  <br><br>
  <a name="RainMachineset"></a>
  <b>set</b>
  <ul>
    <li>getDeviceState - Starts a Datarequest</li>
    <li>getToken - Gets a new Session-ID</li>
  </ul>
  <br><br>
  <a name="RainMachineattributes"></a>
  <b>Attributes</b>
  <ul>
    <li>debugJSON - </li>
    <li>disable - Disables the Bridge</li>
    <li>interval - Interval in seconds (Default=300)</li>
  </ul>
</ul>

=end html
=begin html_DE

<a name="RainMachine"></a>
<h3>RainMachine</h3>
<ul>
  <u><b>Voraussetzungen</b></u>
  <br><br>
  <li>Zusammen mit dem Device RainMachineDevice stellt dieses FHEM Modul die Kommunikation zwischen der GardenaCloud und Fhem her. Es k&ouml;nnen damit Rasenm&auml;her, Bew&auml;sserungscomputer und Bodensensoren überwacht und gesteuert werden</li>
  <li>Das Perl-Modul "SSL Packet" wird ben&ouml;tigt.</li>
  <li>Unter Debian (basierten) System, kann dies mittels "apt-get install libio-socket-ssl-perl" installiert werden.</li>
  <li>Das Gardena-Gateway und alle damit verbundenen Ger&auml;te und Sensoren m&uuml;ssen vorab in der GardenaApp eingerichtet sein.</li>
</ul>
<br>
<a name="RainMachinedefine"></a>
<b>Define</b>
<ul><br>
  <code>define &lt;name&gt; RainMachine &lt;Account-EMail&gt; &lt;Account-Passwort&gt;</code>
  <br><br>
  Beispiel:
  <ul><br>
    <code>define Gardena_Bridge RainMachine me@email.me secret</code><br>
  </ul>
  <br>
  &lt;Account-EMail&gt; Email Adresse, die auch in der GardenaApp verwendet wurde<br>
  &lt;Account-Passwort&gt; Passwort, welches in der GardenaApp verwendet wurde<br>
  Das Bridge Device wird im Raum RainMachine angelegt und danach erfolgt das Einlesen und automatische Anlegen der Ger&auml;te. Von nun an k&ouml;nnen die eingebundenen Ger&auml;te gesteuert werden. &Auml;nderungen in der APP werden mit den Readings und dem Status syncronisiert.
  <br><br>
  <a name="RainMachinereadings"></a>
  <br><br>
  <b>Readings</b>
  <ul>
    <li>address - Adresse, welche in der App eingetragen wurde (Langversion)</li>
    <li>authorized_user_ids - </li>
    <li>city - PLZ, Stadt</li>
    <li>devices - Anzahl der Ger&auml;te, welche in der GardenaCloud angemeldet sind (Gateway z&auml;hlt mit)</li>
    <li>lastRequestState - Letzter abgefragter Status der Bridge</li>
    <li>latitude - Breitengrad des Grundst&uuml;cks</li>
    <li>longitude - Längengrad des Grundst&uuml;cks</li>
    <li>name - Name für das Grundst&uuml;ck – Default „My Garden“</li>
    <li>state - Status der Bridge</li>
    <li>token - SessionID</li>
    <li>zones - </li>
  </ul>
  <br><br>
  <a name="RainMachineset"></a>
  <b>set</b>
  <ul>
    <li>getDeviceState - Startet eine Abfrage der Daten.</li>
    <li>getToken - Holt eine neue Session-ID</li>
  </ul>
  <br><br>
  <a name="RainMachineattributes"></a>
  <b>Attribute</b>
  <ul>
    <li>debugJSON - JSON Fehlermeldungen</li>
    <li>disable - Schaltet die Daten&uuml;bertragung der Bridge ab</li>
    <li>interval - Abfrageinterval in Sekunden (default: 300)</li>
  </ul>
</ul>

=end html_DE
=cut
