###############################################################################
# 
#  (c) 2018 Copyright: Marcus Beckerle (marcus.beckerle at gmx dot de) 
#  All rights reserved
#
#
#  This script is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  The GNU General Public License can be found at
#  http://www.gnu.org/copyleft/gpl.html.
#  A copy is found in the textfile GPL.txt and important notices to the license
#  from the author is found in LICENSE.txt distributed with these scripts.
#
#  This script is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#
# $Id: 74_RainMachineDevice.pm 15650 2017-12-20 05:41:22Z CoolTux $
#
###############################################################################
##
##
## Das JSON Modul immer in einem eval aufrufen
# $data = eval{decode_json($data)};
#
# if($@){
#   Log3($SELF, 2, "$TYPE ($SELF) - error while request: $@");
#  
#   readingsSingleUpdate($hash, "state", "error", 1);
#
#   return;
# }
#
#   
##
##

package main;

my $missingModul = "";

use strict;
use warnings;
use Time::Local;

eval "use JSON;1" or $missingModul .= "JSON ";

my $version = "0.4.0";

# Declare functions
sub RainMachineDevice_Attr(@);
sub RainMachineDevice_Define($$);
sub RainMachineDevice_Initialize($);
sub RainMachineDevice_Set($@);
sub RainMachineDevice_Undef($$);
sub RainMachineDevice_WriteReadings($$);
sub RainMachineDevice_Parse($$);
sub RainMachineDevice_ReadingLangGer($$);
sub RainMachineDevice_RigRadingsValue($$);
sub RainMachineDevice_Zulu2LocalString($);

# Initialization of the device
sub RainMachineDevice_Initialize($) {

    my ($hash) = @_;

    # When a zone is forwarded from the RainMachine, this expression checks if the passed expression is accepted
    # $hash->{Match}      = '^{.*"uid":".*';
    $hash->{Match}      = '.*';

    $hash->{SetFn}      = "RainMachineDevice_Set";
    $hash->{DefFn}      = "RainMachineDevice_Define";
    $hash->{UndefFn}    = "RainMachineDevice_Undef";
    $hash->{ParseFn}    = "RainMachineDevice_Parse";
    $hash->{AttrFn}     = "RainMachineDevice_Attr";

    # The attributes and the
    $hash->{AttrList}   = "readingValueLanguage:de,en ".
                            "model ".
                            "IODev ".
                            $readingFnAttributes;
    
    foreach my $d(sort keys %{$modules{RainMachineDevice}{defptr}}) {
        my $hash = $modules{RainMachineDevice}{defptr}{$d};
        $hash->{VERSION}      = $version;
    }
}

# Called during definition of the device
sub RainMachineDevice_Define($$) {

    my ( $hash, $def ) = @_;
    my @a = split( "[ \t]+", $def );

    Log3 "test", 3, "RainMachineDevice () - defining 1";

    return "too few parameters: define <NAME> RainMachineDevice <device_Id> <model>" if( @a < 3 ) ;
    return "Cannot define RainMachine device. Perl modul $missingModul is missing." if ( $missingModul );


    Log3 "test", 3, "RainMachineDevice () - defining 2";

    my $name            = $a[0];
    my $deviceId        = $a[2];
    my $typeId        = $a[3];
    
    $hash->{DEVICEID}   = $deviceId;
    $hash->{VERSION}    = $version;

    
    # ???
    CommandAttr(undef,"$name IODev $modules{RainMachineBridge}{defptr}{BRIDGE}->{NAME}") if(AttrVal($name,'IODev','none') eq 'none');

    my $iodev           = AttrVal($name,'IODev','none');
    
    AssignIoPort($hash,$iodev) if( !$hash->{IODev} );
    
    if(defined($hash->{IODev}->{NAME})) {
        Log3 $name, 3, "RainMachineDevice ($name) - I/O device is " . $hash->{IODev}->{NAME};
    } else {
        Log3 $name, 1, "RainMachineDevice ($name) - no I/O device";
    }
    
    $iodev = $hash->{IODev}->{NAME};
    
    my $d = $modules{RainMachineDevice}{defptr}{$deviceId};
    
    return "RainMachineDevice device $name on RainMachineBridge $iodev already defined."
    if( defined($d) && $d->{IODev} == $hash->{IODev} && $d->{NAME} ne $name );
    
    
    $attr{$name}{room}          = "RainMachine"    if( not defined( $attr{$name}{room} ) );
    $attr{$name}{model}         = $typeId         if( not defined( $attr{$name}{model} ) );
    
    Log3 $name, 3, "RainMachineDevice ($name) - defined RainMachineDevice with DEVICEID: $deviceId";
    readingsSingleUpdate($hash,'state','initialized',1);
    
    $modules{RainMachineDevice}{defptr}{$deviceId} = $hash;

    return undef;
}

# Method called if undefining device
sub RainMachineDevice_Undef($$) {

    my ( $hash, $arg )  = @_;
    my $name            = $hash->{NAME};
    my $deviceId        = $hash->{DEVICEID};

    delete $modules{RainMachineDevice}{defptr}{$deviceId};

    return undef;
}

# Method called when attribute is set
sub RainMachineDevice_Attr(@) {

    my ( $cmd, $name, $attrName, $attrVal ) = @_;
    my $hash = $defs{$name};


    return undef;
}

# Method called when setting is called
sub RainMachineDevice_Set($@) {
    
    my ($hash, $name, $cmd, @args) = @_;
    my ($arg, @params) = @args;
    
    my $payload;
    my $abilities;
    
    # TODO: change to zone settings
    ### mower
    if( lc $cmd eq 'start_zone' ) {


        $payload    = 'start_zone';
    
    } elsif( lc $cmd eq 'stop_zone' ) {
    
        $payload    = 'stop_zone';

    } else {
    
        my $list    = '';
        $list       .= 'start_zone:noArg stop_zone:noArg' if( AttrVal($name,'model','unknown') eq '2' );
        #$list       .= 'manualOverride:slider,0,1,59 cancelOverride:noArg' if( AttrVal($name,'model','unknown') eq 'watering_computer' );
        #$list       .= 'refresh:temperature,light' if( AttrVal($name,'model','unknown') eq 'sensor' );
        
        return "Unknown argument $cmd, choose one of $list";
    }
    
    #$abilities  = 'mower' if( AttrVal($name,'model','unknown') eq 'mower' );
    #$abilities  = 'outlet' if( AttrVal($name,'model','unknown') eq 'watering_computer' );
    
    
    $hash->{helper}{deviceAction}  = $payload;
    readingsSingleUpdate( $hash, "state", "send command to RainMachine", 1);
    
    IOWrite($hash,$payload,$hash->{DEVICEID});
    Log3 $name, 4, "RainMachineBridge ($name) - IOWrite: $payload $hash->{DEVICEID} $abilities IODevHash=$hash->{IODev}";
    
    return undef;
}

# Parse API response
sub RainMachineDevice_Parse($$) {

    my ($io_hash,$json)  = @_;
    my $name                    = $io_hash->{NAME};

    Log3 $name, 3, "RainMachineDevice ($name) - parsing";
    
    my $decode_json =   eval{decode_json($json)};
    if($@){
        Log3 $name, 3, "RainMachineBridge ($name) - JSON error while request: $@";
    }
    
    Log3 $name, 4, "RainMachineDevice ($name) - ParseFn was called";
    Log3 $name, 5, "RainMachineDevice ($name) - JSON: $json";

    
    if( defined($decode_json->{uid}) ) {

        my $deviceId                = $decode_json->{uid};

        # 2 is ZONE
        my $typeId                = $decode_json->{type};

        # If there is already a sub device for the zone, use it, else autocreate new device fot the zone
        if( my $hash                = $modules{RainMachineDevice}{defptr}{$deviceId} ) {  
            my $name                = $hash->{NAME};
                        
            RainMachineDevice_WriteReadings($hash,$decode_json);
            Log3 $name, 3, "RainMachineDevice ($name) - find logical device: $hash->{NAME}";
                        
            return $hash->{NAME};
            
        } else {
            if ($deviceId == 5) {
                Log3 $name, 3, "RainMachineDevice ($name) - autocreate new device " . makeDeviceName($deviceId) . " with deviceId $decode_json->{uid}, model $typeId";
                return "UNDEFINED " . makeDeviceName("zone_$deviceId") . " RainMachineDevice $decode_json->{uid} $typeId";
            }
        }
    }
}

# Read the values/attributes from API-JSON-Response
sub RainMachineDevice_WriteReadings($$) {

    my ($hash,$decode_json)     = @_;
    
    my $name                    = $hash->{NAME};
    
    readingsBeginUpdate($hash);

    # Zones
    readingsBulkUpdate($hash,'name',$decode_json->{name}) if( AttrVal($name,'model','unknown') eq '2' );
    readingsBulkUpdate($hash,'state',$decode_json->{state}) if( AttrVal($name,'model','unknown') eq '2' );
    readingsBulkUpdate($hash,'active',$decode_json->{active}) if( AttrVal($name,'model','unknown') eq '2' );
    readingsBulkUpdate($hash,'userDuration',$decode_json->{userDuration}) if( AttrVal($name,'model','unknown') eq '2' );
    readingsBulkUpdate($hash,'machineDuration',$decode_json->{machineDuration}) if( AttrVal($name,'model','unknown') eq '2' );
    readingsBulkUpdate($hash,'remaining',$decode_json->{remaining}) if( AttrVal($name,'model','unknown') eq '2' );
    readingsBulkUpdate($hash,'cycle',$decode_json->{cycle}) if( AttrVal($name,'model','unknown') eq '2' );
    readingsBulkUpdate($hash,'noOfCycles',$decode_json->{noOfCycles}) if( AttrVal($name,'model','unknown') eq '2' );
    readingsBulkUpdate($hash,'master',$decode_json->{master}) if( AttrVal($name,'model','unknown') eq '2' );
    readingsBulkUpdate($hash,'waterSense',$decode_json->{waterSense}) if( AttrVal($name,'model','unknown') eq '2' );

    readingsEndUpdate( $hash, 1 );
    
    Log3 $name, 4, "RainMachineDevice ($name) - readings was written}";
}

# ???
sub RainMachineDevice_ReadingLangGer($$) {

    my ($hash,$readingValue)    = @_;
    my $name                    = $hash->{NAME};
    
    
    my %langMap = (
                '0'                        =>  'an',
                '1'                        =>  'aus'
    );
    
    if( defined($langMap{$readingValue}) and (AttrVal('global','language','none') eq 'DE' or AttrVal($name,'readingValueLanguage','none') eq 'de') and AttrVal($name,'readingValueLanguage','none') ne 'en') {
        return $langMap{$readingValue};
    } else {
        return $readingValue;
    }
}

# ???
sub RainMachineDevice_RigRadingsValue($$) {

    my ($hash,$readingValue)    = @_;

    my $rigReadingValue;
    
    
    if( $readingValue =~ /^(\d+)-(\d\d)-(\d\d)T(\d\d)/ ) {
        $rigReadingValue = RainMachineDevice_Zulu2LocalString($readingValue);
    } else {
        $rigReadingValue = RainMachineDevice_ReadingLangGer($hash,$readingValue);
    }

    return $rigReadingValue;
}

# ???
sub RainMachineDevice_Zulu2LocalString($) {

    my $t = shift;
    my ($datehour,$datemin,$rest) = split(/:/,$t,3);


    my ($year, $month, $day, $hour,$min) = $datehour =~ /(\d+)-(\d\d)-(\d\d)T(\d\d)/;
    my $epoch = timegm (0,0,$hour,$day,$month-1,$year);

    my ($lyear,$lmonth,$lday,$lhour,$isdst) = (localtime($epoch))[5,4,3,2,-1];

    $lyear += 1900;  # year is 1900 based
    $lmonth++;       # month number is zero based

    if( defined($rest) ) {
        return ( sprintf("%04d-%02d-%02d %02d:%02d:%s",$lyear,$lmonth,$lday,$lhour,$datemin,substr($rest,0,2)));
    } elsif( $lyear < 2000 ) {
        return "illegal year";
    } else {
        return ( sprintf("%04d-%02d-%02d %02d:%02d",$lyear,$lmonth,$lday,$lhour,substr($datemin,0,2)));
    }
}






1;

=pod

=item device
=item summary    Modul to control RainMachine Devices
=item summary_DE Modul zur Steuerung von RainMachineger&aumlten

=begin html

<a name="RainMachineDevice"></a>
<h3>RainMachineDevice</h3>
<ul>
    In combination with RainMachineBridge this FHEM Module controls the RainMachine Device using the GardenaCloud
    <br><br>
    Once the Bridge device is created, the connected devices are automatically recognized and created in FHEM. <br>
    From now on the devices can be controlled and changes in the GardenaAPP are synchronized with the state and readings of the devices.
    <a name="RainMachineDevicereadings"></a>
    <br><br><br>
    <b>Readings</b>
    <ul>
        <li>battery-charging - Indicator if the Battery is charged (0/1) or with newer Firmware (false/true)</li>
        <li>battery-level - load percentage of the Battery</li>
        <li>battery-rechargeable_battery_status - healthyness of the battery (out_of_operation/replace_now/low/ok)</li>
        <li>device_info-category - category of device (mower/watering_computer)</li>
        <li>device_info-last_time_online - timestamp of last radio contact</li>
        <li>device_info-manufacturer - manufacturer</li>
        <li>device_info-product - product type</li>
        <li>device_info-serial_number - serial number</li>
        <li>device_info-sgtin - </li>
        <li>device_info-version - firmware version</li>
        <li>firmware-firmware_command - firmware command (idle/firmware_cancel/firmware_upload/unsupported)</li>
        <li>firmware-firmware_status - firmware status </li>
        <li>firmware-firmware_update_start - indicator when a firmwareupload is started</li>
        <li>firmware-firmware_upload_progress - progress indicator of firmware update</li>
        <li>firmware-inclusion_status - inclusion status</li>
        <li>internal_temperature-temperature - internal device temperature</li>
        <li>mower-error - actual error message
        <ul>
            <li>no_message</li>
            <li>outside_working_area</li>
            <li>no_loop_signal</li>
            <li>wrong_loop_signal</li>
            <li>loop_sensor_problem_front</li>
            <li>loop_sensor_problem_rear</li>
            <li>trapped</li>
            <li>upside_down</li>
            <li>low_battery</li>
            <li>empty_battery</li>
            <li>no_drive</li>
            <li>lifted</li>
            <li>stuck_in_charging_station</li>
            <li>charging_station_blocked</li>
            <li>collision_sensor_problem_rear</li>
            <li>collision_sensor_problem_front</li>
            <li>wheel_motor_blocked_right</li>
            <li>wheel_motor_blocked_left</li>
            <li>wheel_drive_problem_right</li>
            <li>wheel_drive_problem_left</li>
            <li>cutting_system_blocked</li>
            <li>invalid_sub_device_combination</li>
            <li>settings_restored</li>
            <li>electronic_problem</li>
            <li>charging_system_problem</li>
            <li>tilt_sensor_problem</li>
            <li>wheel_motor_overloaded_right</li>
            <li>wheel_motor_overloaded_left</li>
            <li>charging_current_too_high</li>
            <li>temporary_problem</li>
            <li>guide_1_not_found</li>
            <li>guide_2_not_found</li>
            <li>guide_3_not_found</li>
            <li>difficult_finding_home</li>
            <li>guide_calibration_accomplished</li>
            <li>guide_calibration_failed</li>
            <li>temporary_battery_problem</li>
            <li>battery_problem</li>
            <li>alarm_mower_switched_off</li>
            <li>alarm_mower_stopped</li>
            <li>alarm_mower_lifted</li>
            <li>alarm_mower_tilted</li>
            <li>connection_changed</li>
            <li>connection_not_changed</li>
            <li>com_board_not_available</li>
            <li>slipped</li>
        </ul>
        </li>
        <li>mower-manual_operation - (0/1) or with newer Firmware (false/true)</li>
        <li>mower-override_end_time - manual override end time</li>
        <li>mower-source_for_next_start - source for the next start
        <ul>
            <li>no_source</li>
            <li>mower_charging</li>
            <li>completed_cutting_autotimer</li>
            <li>week_timer</li>
            <li>countdown_timer</li>
            <li>undefined</li>
        </ul>
        </li>  
        <li>mower-status - mower state (see state)</li>
        <li>mower-timestamp_next_start - timestamp of next scheduled start</li>
        <li>radio-connection_status - state of connection</li>
        <li>radio-quality - percentage of the radio quality</li>
        <li>radio-state - radio state (bad/poor/good/undefined)</li>
        <li>state - state of the mower
        <ul>
            <li>paused</li>
            <li>ok_cutting</li>
            <li>ok_searching</li>
            <li>ok_charging</li>
            <li>ok_leaving</li>
            <li>wait_updating</li>
            <li>wait_power_up</li>
            <li>parked_timer</li>
            <li>parked_park_selected</li>
            <li>off_disabled</li>
            <li>off_hatch_open</li>
            <li>unknown</li>
            <li>error</li>
            <li>error_at_power_up</li>
            <li>off_hatch_closed</li>
            <li>ok_cutting_timer_overridden</li>
            <li>parked_autotimer</li>
            <li>parked_daily_limit_reached</li>
        </ul>
        </li>
    </ul>
    <br><br>
    <a name="RainMachineDeviceattributes"></a>
    <b>Attributes</b>
    <ul>
        <li>readingValueLanguage - Change the Language of Readings (de,en/if not set the default is english and the global language is not set at german) </li>
        <li>model - </li>
    </ul>
    <br><br>
    <a name="RainMachineDeviceset"></a>
    <b>set</b>
    <ul>
        <li>parkUntilFurtherNotice</li>
        <li>parkUntilNextTimer</li>
        <li>startOverrideTimer - 0 to 59 Minutes</li>
        <li>startResumeSchedule</li>
    </ul>
</ul>

=end html
=begin html_DE

<a name="RainMachineDevice"></a>
<h3>RainMachineDevice</h3>
<ul>
    Zusammen mit dem Device RainMachineDevice stellt dieses FHEM Modul die Kommunikation zwischen der GardenaCloud und Fhem her.
    <br><br>
    Wenn das RainMachineBridge Device erzeugt wurde, werden verbundene Ger&auml;te automatisch erkannt und in Fhem angelegt.<br> 
    Von nun an k&ouml;nnen die eingebundenen Ger&auml;te gesteuert werden. &Auml;nderungen in der APP werden mit den Readings und dem Status syncronisiert.
    <a name="RainMachineDevicereadings"></a>
    </ul>
    <br>
    <ul>
    <b>Readings</b>
    <ul>
        <li>battery-charging - Ladeindikator (0/1) oder mit neuerer Firmware (false/true)</li>
        <li>battery-level - Ladezustand der Batterie in Prozent</li>
        <li>battery-rechargeable_battery_status - Zustand der Batterie (Ausser Betrieb/Kritischer Batteriestand, wechseln Sie jetzt/Niedrig/oK)</li>
        <li>device_info-category - Eigenschaft des Ger&auml;tes (M&auml;her/Bew&auml;sserungscomputer/Bodensensor)</li>
        <li>device_info-last_time_online - Zeitpunkt der letzten Funk&uuml;bertragung</li>
        <li>device_info-manufacturer - Hersteller</li>
        <li>device_info-product - Produkttyp</li>
        <li>device_info-serial_number - Seriennummer</li>
        <li>device_info-sgtin - </li>
        <li>device_info-version - Firmware Version</li>
        <li>firmware-firmware_command - Firmware Kommando (Nichts zu tun/Firmwareupload unterbrochen/Firmwareupload/nicht unterst&uuml;tzt)</li>
        <li>firmware-firmware_status - Firmware Status </li>
        <li>firmware-firmware_update_start - Firmwareupdate (0/1) oder mit neuerer Firmware (false/true)</li>
        <li>firmware-firmware_upload_progress - Firmwareupdatestatus in Prozent</li>
        <li>firmware-inclusion_status - Einbindungsstatus</li>
        <li>internal_temperature-temperature - Interne Ger&auml;te Temperatur</li>
        <li>mower-error - Aktuelle Fehler Meldung
        <ul>
            <li>Kein Fehler</li>
            <li>Au&szlig;erhalb des Arbeitsbereichs</li>
            <li>Kein Schleifensignal</li>
            <li>Falsches Schleifensignal</li>
            <li>Problem Schleifensensor, vorne</li>
            <li>Problem Schleifensensor, hinten</li>
            <li>Eingeschlossen</li>
            <li>Steht auf dem Kopf</li>
            <li>Niedriger Batteriestand</li>
            <li>Batterie ist leer</li>
            <li>Kein Antrieb</li>
            <li>Angehoben</li>
            <li>Eingeklemmt in Ladestation</li>
            <li>Ladestation blockiert</li>
            <li>Problem Sto&szlig;sensor hinten</li>
            <li>Problem Sto&szlig;sensor vorne</li>
            <li>Radmotor rechts blockiert</li>
            <li>Radmotor links blockiert</li>
            <li>Problem Antrieb, rechts</li>
            <li>Problem Antrieb, links</li>
            <li>Schneidsystem blockiert</li>
            <li>Fehlerhafte Verbindung</li>
            <li>Standardeinstellungen</li>
            <li>Elektronisches Problem</li>
            <li>Problem Ladesystem</li>
            <li>Kippsensorproblem</li>
            <li>Rechter Radmotor &uuml;berlastet</li>
            <li>Linker Radmotor &uuml;berlastet</li>
            <li>Ladestrom zu hoch</li>
            <li>Vor&uuml;bergehendes Problem</li>
            <li>SK 1 nicht gefunden</li>
            <li>SK 2 nicht gefunden</li>
            <li>SK 3 nicht gefunden</li>
            <li>Problem die Ladestation zu finden</li>
            <li>Kalibration des Suchkabels beendet</li>
            <li>Kalibration des Suchkabels fehlgeschlagen</li>
            <li>Kurzzeitiges Batterieproblem</li>
            <li>Batterieproblem</li>
            <li>Alarm! M&auml;her ausgeschalten</li>
            <li>Alarm! M&auml;her gestoppt</li>
            <li>Alarm! M&auml;her angehoben</li>
            <li>Alarm! M&auml;her gekippt</li>
            <li>Verbindung geändert</li>
            <li>Verbindung nicht ge&auml;ndert</li>
            <li>COM board nicht verf&uuml;gbar</li>
            <li>Rutscht</li>
        </ul>
        </li>
        <li>mower-manual_operation - Manueller Betrieb (0/1) oder mit neuerer Firmware (false/true)</li>
        <li>mower-override_end_time - Zeitpunkt wann der manuelle Betrieb beendet ist</li>
        <li>mower-source_for_next_start - Grund f&uuml;r den n&auml;chsten Start
        <ul>
            <li>Kein Grund</li>
            <li>M&auml;her wurde geladen</li>
            <li>SensorControl erreicht</li>
            <li>Wochentimer erreicht</li>
            <li>Stoppuhr Timer</li>
            <li>Undefiniert</li>
        </ul>
        </li>  
        <li>mower-status - M&auml;her Status (siehe state)</li>
        <li>mower-timestamp_next_start - Zeitpunkt des n&auml;chsten geplanten Starts</li>
        <li>radio-connection_status - Status der Funkverbindung</li>
        <li>radio-quality - Indikator f&uuml;r die Funkverbindung in Prozent</li>
        <li>radio-state - radio state (schlecht/schwach/gut/Undefiniert)</li>
        <li>state - Staus des M&auml;hers
        <ul>
            <li>Pausiert</li>
            <li>M&auml;hen</li>
            <li>Suche Ladestation</li>
            <li>L&auml;dt</li>
            <li>M&auml;hen</li>
            <li>Wird aktualisiert ...</li>
            <li>Wird eingeschaltet ...</li>
            <li>Geparkt nach Zeitplan</li>
            <li>Geparkt</li>
            <li>Der M&auml;her ist ausgeschaltet</li>
            <li>Deaktiviert. Abdeckung ist offen oder PIN-Code erforderlich</li>
            <li>Unbekannter Status</li>
            <li>Fehler</li>
            <li>Neustart ...</li>
            <li>Deaktiviert. Manueller Start erforderlich</li>
            <li>Manuelles M&auml;hen</li>
            <li>Geparkt durch SensorControl</li>
            <li>Abgeschlossen</li>
        </ul>
        </li>
    </ul>
    <br><br>
    <a name="RainMachineDeviceattributes"></a>
    <b>Attribute</b>
    <ul>
        <li>readingValueLanguage - &Auml;nderung der Sprache der Readings (de,en/wenn nichts gesetzt ist, dann Englisch es sei denn deutsch ist als globale Sprache gesetzt) </li>
        <li>model - </li>
    </ul>
    <a name="RainMachineDeviceset"></a>
    <b>set</b>
    <ul>
        <li>parkUntilFurtherNotice - Parken des M&auml;hers unter Umgehung des Zeitplans</li>
        <li>parkUntilNextTimer - Parken bis zum n&auml;chsten Zeitplan</li>
        <li>startOverrideTimer - Manuelles m&auml;hen (0 bis 59 Minuten)</li>
        <li>startResumeSchedule - Weiterf&uuml;hrung des Zeitplans</li>
    </ul>
</ul>

=end html_DE
=cut
